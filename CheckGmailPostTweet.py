import poplib, email, getpass, imaplib, os, tweepy, time, tempfile

tmpdir = tempfile.mkdtemp()
m = imaplib.IMAP4_SSL("imap.gmail.com")
#Gmail Keys
user = ("...@gmail.com")
pwd = ('...')
#Twitter Keys
cfg = { 
"consumer_key"        : "...",
"consumer_secret"     : "...",
"access_token"        : "...",
"access_token_secret" : "..." 
}


m.login(user,pwd)#log in for Gmail API
print 'Logged in to GMail'
m.select("INBOX") # here you a can choose a mail box like INBOX instead
# use m.list() to get all the mailboxes

resp, items = m.search(None, "UnSeen") #filter using the IMAP rules here (check http://www.example-code.com/csharp/imap-search-critera.asp)
items = items[0].split() # getting the mails id

def get_api(cfg): #authenticate for the Twitter portion
  auth = tweepy.OAuthHandler(cfg['consumer_key'], cfg['consumer_secret'])
  auth.set_access_token(cfg['access_token'], cfg['access_token_secret'])
  return tweepy.API(auth)
print 'Logged in to Twitter'
print ('Total number of new emails: ' +str(len(items)))
for emailid in items: #iterates through all emails
    resp, data = m.fetch(emailid, "(RFC822)") # fetching the mail, "`(RFC822)`" means get everything
    email_body = data[0][1] # getting the mail content
    mail = email.message_from_string(email_body) # parsing the mail content to get a mail object
    print emailid
    if mail["From"].find('Transnomis Notification') > 0 and mail["Subject"].find('Issue Created') > 0: #filtering only created events from transnomis
        print ('Filtered emailed dated: '+mail["Date"])      
        for part in mail.walk(): #some error checking in case the email doesn't come through with an image
            if part.get_content_maintype() != 'multipart' and part.get('Content-Disposition') is not None:
                image_name =  part.get_filename()  + ".jpg"
                open(tmpdir + image_name, 'wb').write(part.get_payload(decode=True))
            else:
                continue
            
        api = get_api(cfg) #calling the twitter api
        #below is the actual tweet (image path, text portion)
        status = api.update_with_media(tmpdir + image_name , mail["Subject"][mail["Subject"].find('Issue Created'):].split('#')[0].replace('\r\n','') +'\r\n'+ mail["Subject"][mail["Subject"].find('Issue Created'):].split('#')[1].split(' ',1)[1]+'\r\n'+'For more information please see: https://roads.grey.ca/') 
        #Have to add in a delay to prevent twitter from banning the account
        time.sleep(5)
    elif mail["From"].find('Transnomis Notification') >0 and mail["Subject"].find('Issue Ended') > 0: #filtering only ended events from transnomis
        print ('Filtered emailed dated: '+mail["Date"])   
        api = get_api(cfg) #calling the twitter api
        #below is the actual tweet (image path, text portion)
        status = api.update_status(mail["Subject"][mail["Subject"].find('Issue Ended'):].split('#')[0].replace('\r\n','') +'\r\n'+ mail["Subject"][mail["Subject"].find('Issue Ended'):].split('#')[1].split(' ',1)[1]+'\r\n'+'For more information please see: https://roads.grey.ca/') 
        #Have to add in a delay to prevent twitter from banning the account
        time.sleep(5)
    
try:
  m.store(items[0].replace(' ',','),'+FLAGS','\SEEN') #marks emails as read when script complete
except:
  pass

