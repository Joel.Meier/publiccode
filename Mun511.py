'''
Municipal511.py
This script has been created to pull data down from Municipal 511.
Created by: Joel Meier (date not documented)
Change log
    Modified: Joel Meier 26/02/2019
        Fixed formatting and updated comments
    Modified: Jody MacEachern 25/02/2019
        Changed GET requests to post. 
'''
import xml.etree.ElementTree as ET
import urllib3
import arcpy
import polyline
import requests
import os
import datetime

# In order to authenticate the requests to get the data first a token needs to be recevied.
s = requests.Session()
r = s.post(
    'URL1')

print(r)
token = r.text.split("\"")
# Token received from data feed and placed into parametes to sign in.
payload = {
    'auditId': token[3],
    'username': '',
    'password': ''}
# testing # payload = {'auditId' : token [3], 'username' : 'GreyCtyAdmin', 'password' : '31HVKyZtjC'}
s2 = s.post(
    'URL2',
    params=payload)
print(s2.text)  # should read: u'{"Success":true,"Message":"OK"}'
r2 = s.post(
    'URL3?div=5000',
    params=payload)
r1 = s.post(
    'URL3?div=8010',
    params=payload)

# Each div is a different layer in Mun511.
# 5000 is road issues public, 8010 is Road Restrictions public.
print(r2)
print(r1)
# Changed to r2.content from .text as there were issues with encoding.
root = ET.fromstring(r2.content)
root1 = ET.fromstring(r1.content)
# Define database connections
fcl = "DatabaseConnections"
fcp = "DatabaseConnections"
fcpoly = "CDatabaseConnections"
# Clearing Data within feature classes
arcpy.DeleteRows_management(fcl)
arcpy.DeleteRows_management(fcp)
arcpy.DeleteRows_management(fcpoly)

# Iterate through all issues in "Road Issues Public"
for x in root.iter('Issue'):  # Road issues public Line
    #testfile.write("ID: %s Time: %s\n" % (x.attrib.values()[0], str(datetime.datetime.now())))
    issues = 0
    try:
        IssueType = 'Road Issue - ' + x.find('IssueType').text
    except BaseException:
        IssueType = ''
    #testfile.write("\tIssueType: %s Time: %s\n" % (IssueType, str(datetime.datetime.now())))

    try:
        Description = x.find('Description').text
    except BaseException:
        Description = ''
    #testfile.write("\tDescription: %s Time: %s\n" % (Description, str(datetime.datetime.now())))

    try:
        PlanStartLocal = x.find('PlanStartLocal').text
    except BaseException:
        try:
            PlanStartLocal = x.find('DataLocalTime').text
        except BaseException:
            PlanStartLocal = None
            print('no start date')
    try:
        PlanEndLocal = x.find('PlanEndLocal').text
    except BaseException:
        PlanEndLocal = None
        print('no end date')

    for y in x.iter('Location'):  # Issues may have multiple locations
        try:
            Location = y.find('MainRoad').text
        except BaseException:
            Location = None
        #testfile.write("%s %s\n" % (Location, str(datetime.datetime.now())))
        LocationType = y.find('LocationType').text
        GoogleEncodedPolyline = y.findtext('.//GoogleEncodedPolyline')
        print('     ' + (LocationType))
        cur = arcpy.da.InsertCursor(fcl,
                                    ["LocationType",
                                     "IssueType",
                                     "Description",
                                     "PlanStartLocal",
                                     "PlanEndLocal",
                                     "Location",
                                     "SHAPE@"])
        array = arcpy.Array()
        if GoogleEncodedPolyline is not None and Description is not None:
            for coords in polyline.decode(GoogleEncodedPolyline):
                array.add(arcpy.Point(coords[1], coords[0]))
                if issues == 0:
                    issues += 1
            cur.insertRow([LocationType,
                           IssueType,
                           Description,
                           PlanStartLocal,
                           PlanEndLocal,
                           Location,
                           arcpy.Polyline(array)])

        else:
            pass

        del cur
for x in root1.iter('Issue'):  # Road restrictions public line
    issues = 0
    try:
        IssueType = 'Restriction - ' + x.find('IssueType').text
    except BaseException:
        IssueType = ''
    try:
        Description = x.find('Description').text
    except BaseException:
        Description = ''
    print(Description)
    try:
        PlanStartLocal = x.find('PlanStartLocal').text
    except BaseException:
        try:
            PlanStartLocal = x.find('DataLocalTime').text
        except BaseException:
            PlanStartLocal = None
            print('no start date')
    try:
        PlanEndLocal = x.find('PlanEndLocal').text
    except BaseException:
        PlanEndLocal = None
        print('no end date')
    for y in x.iter('Location'):
        try:
            Location = y.find('MainRoad').text
        except BaseException:
            Location = None
        LocationType = y.find('LocationType').text
        GoogleEncodedPolyline = y.findtext('.//GoogleEncodedPolyline')
        print('     ' + (LocationType))
        cur = arcpy.da.InsertCursor(fcl,
                                    ["LocationType",
                                     "IssueType",
                                     "Description",
                                     "PlanStartLocal",
                                     "PlanEndLocal",
                                     "Location",
                                     "SHAPE@"])
        array = arcpy.Array()
        if GoogleEncodedPolyline is not None and Description is not None:
            for coords in polyline.decode(GoogleEncodedPolyline):
                array.add(arcpy.Point(coords[1], coords[0]))
                if issues == 0:
                    issues += 1
            cur.insertRow([LocationType,
                           IssueType,
                           Description,
                           PlanStartLocal,
                           PlanEndLocal,
                           Location,
                           arcpy.Polyline(array)])

        else:
            pass

        del cur

for x in root.iter('Issue'):  # Road issues Public Point
    issues = 0
    try:
        IssueType = 'Road Issue - ' + x.find('IssueType').text
    except BaseException:
        IssueType = ''
    try:
        Description = x.find('Description').text
    except BaseException:
        Description = ''
    try:
        PlanStartLocal = x.find('PlanStartLocal').text
    except BaseException:
        try:
            PlanStartLocal = x.find('DataLocalTime').text
        except BaseException:
            PlanStartLocal = None
            print('no start date')
    try:
        PlanEndLocal = x.find('PlanEndLocal').text
    except BaseException:
        PlanEndLocal = None
        print('no end date')
    for y in x.iter('Location'):
        try:
            Location = y.find('MainRoad').text
        except BaseException:
            Location = None
        LocationType = y.find('LocationType').text
        GoogleEncodedPolyline = y.findtext('.//GoogleEncodedPolyline')
        print('     ' + (LocationType))
        print(issues)
        cur2 = arcpy.da.InsertCursor(fcp,
                                     ["LocationType",
                                      "IssueType",
                                      "Description",
                                      "PlanStartLocal",
                                      "PlanEndLocal",
                                      "Location",
                                      "SHAPE@XY"])

        array = arcpy.Array()
        if GoogleEncodedPolyline is not None and Description is not None:
            for coords in polyline.decode(GoogleEncodedPolyline):
                array.add(arcpy.Point(coords[1], coords[0]))
                if issues == 0:
                    cur2.insertRow([LocationType,
                                    IssueType,
                                    Description,
                                    PlanStartLocal,
                                    PlanEndLocal,
                                    Location,
                                    arcpy.Point(coords[1],
                                                coords[0])])
                    issues += 1

        else:
            pass
        del cur2

for x in root1.iter('Issue'):  # Road restrictions public point
    issues = 0
    try:
        IssueType = 'Restriction - ' + x.find('IssueType').text
    except BaseException:
        IssueType = ''
    try:
        Description = x.find('Description').text
    except BaseException:
        Description = ''
    try:
        PlanStartLocal = x.find('PlanStartLocal').text
    except BaseException:
        try:
            PlanStartLocal = x.find('DataLocalTime').text
        except BaseException:
            PlanStartLocal = None
            print('no start date')
    try:
        PlanEndLocal = x.find('PlanEndLocal').text
    except BaseException:
        PlanEndLocal = None
        print('no end date')
    for y in x.iter('Location'):
        try:
            Location = y.find('MainRoad').text
        except BaseException:
            Location = None
        LocationType = y.find('LocationType').text
        GoogleEncodedPolyline = y.findtext('.//GoogleEncodedPolyline')
        print('     ' + (LocationType))
        print(issues)
        cur2 = arcpy.da.InsertCursor(fcp,
                                     ["LocationType",
                                      "IssueType",
                                      "Description",
                                      "PlanStartLocal",
                                      "PlanEndLocal",
                                      "Location",
                                      "SHAPE@XY"])

        array = arcpy.Array()
        if GoogleEncodedPolyline is not None and Description is not None:
            for coords in polyline.decode(GoogleEncodedPolyline):
                array.add(arcpy.Point(coords[1], coords[0]))
                if issues == 0:
                    cur2.insertRow([LocationType,
                                    IssueType,
                                    Description,
                                    PlanStartLocal,
                                    PlanEndLocal,
                                    Location,
                                    arcpy.Point(coords[1],
                                                coords[0])])
                    issues += 1

        else:
            pass
        del cur2

for x in root.iter('Issue'):  # Road isues public polygon
    if x.find('Polygon') is not None:
        try:
            IssueType = 'Road Issue - ' + x.find('IssueType').text
        except BaseException:
            IssueType = ''
        try:
            Description = x.find('Description').text
        except BaseException:
            Description = ''
        try:
            PlanStartLocal = x.find('PlanStartLocal').text
        except BaseException:
            try:
                PlanStartLocal = x.find('DataLocalTime').text
            except BaseException:
                PlanStartLocal = None
                print('no start date')
        try:
            PlanEndLocal = x.find('PlanEndLocal').text
        except BaseException:
            PlanEndLocal = None
            print('no end date')
        try:
            LocationType = x.find('LocationType').text
        except BaseException:
            LocationType = None
            print('no LocationType')
        try:
            Location = 'Grey County'
        except BaseException:
            Location = None
            print('no end date')
        for y in x.findall('Polygon'):
            GoogleEncodedPolyline = y.text
            array = []
            if GoogleEncodedPolyline is not None and Description is not None:
                for coords in polyline.decode(GoogleEncodedPolyline):
                    array.append((coords[1], coords[0]))
                cur3 = arcpy.da.InsertCursor(fcpoly,
                                             ["LocationType",
                                              "IssueType",
                                              "Description",
                                              "PlanStartLocal",
                                              "PlanEndLocal",
                                              "Location",
                                              "SHAPE@"])
                cur3.insertRow([LocationType,
                                IssueType,
                                Description,
                                PlanStartLocal,
                                PlanEndLocal,
                                Location,
                                array])

            else:
                pass

            del cur3
    else:
        pass


for x in root1.iter('Issue'):  # Road restrictions public polygon
    if x.find('Polygon') is not None:
        try:
            IssueType = 'Restriction - ' + x.find('IssueType').text
        except BaseException:
            IssueType = ''
        try:
            Description = x.find('Description').text
        except BaseException:
            Description = ''
        try:
            PlanStartLocal = x.find('PlanStartLocal').text
        except BaseException:
            try:
                PlanStartLocal = x.find('DataLocalTime').text
            except BaseException:
                PlanStartLocal = None
                print('no start date')
        try:
            PlanEndLocal = x.find('PlanEndLocal').text
        except BaseException:
            PlanEndLocal = None
            print('no end date')
        try:
            Location = 'Grey County'
        except BaseException:
            Location = None
            print('no end date')
        for y in x.findall('Polygon'):
            GoogleEncodedPolyline = y.text
            array = []
            #testfile.write("polygon %s\n" % (LocationType, str(datetime.datetime.now())))
            if GoogleEncodedPolyline is not None and Description is not None:
                for coords in polyline.decode(GoogleEncodedPolyline):
                    array.append((coords[1], coords[0]))
                cur3 = arcpy.da.InsertCursor(fcpoly,
                                             ["LocationType",
                                              "IssueType",
                                              "Description",
                                              "PlanStartLocal",
                                              "PlanEndLocal",
                                              "Location",
                                              "SHAPE@"])
                cur3.insertRow([LocationType,
                                IssueType,
                                Description,
                                PlanStartLocal,
                                PlanEndLocal,
                                Location,
                                array])

            else:
                pass

            del cur3
    else:
        pass
