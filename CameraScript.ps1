#!/bin/bash
#Tim Scott Jan 14 V.01

#/usr/local/sbin/Get124.sh

#pull files from gc-dn1
rm /var/www/grey.ca/cameras/building.jpg


#Rename files to friendly format (dirty)
cd /var/www/grey.ca/cameras
mv Grey_County_@_124.jpg 124-9.jpg
mv Grey_County_RD_10_Clifford.jpg clifford.jpg
mv Grey_County_RD_124_@_4.jpg 124-4.jpg
mv Grey_County_RD_17A_@_1.jpg 17a-1.jpg
mv Grey_County_RD_14_@_Hwy_89_Conn_cam_1.jpg conn.jpg
mv Grey_County_RD_14_@_Hwy_89_Conn_cam_2.jpg conn_mto.jpg
mv Grey_County_RD_18_@_29.jpg bognor.jpg
mv Grey_County_Roundabout.jpg roundabout.jpg
mv current.jpg scone.jpg
mv Grey_County_RD_12_@_23 12markdale.jpg

#Adjust Permissions
chown root:www-data /var/www/www.grey.ca/cameras/*.jpg

CAMFILES=/var/www/grey.ca/cameras/*.jpg
for f in $CAMFILES
do
	#Check to see if file is older then 90 minutes
	if test `find $f -mmin +120`
	then
		#If it's too old, copy over the unavailable image.    	
    	cp /var/www/grey.ca/cameras/assets/unavailable.png $f
	fi
	#Line below used for debugging
  	#echo "Processing $f file..."
done 
#Exit Clean
exit 0

#Old script - didn't seem to be working
#Check for old files (90 mins) and copy unavailable over if too old
#for i in {find . -maxdepth 1 -mmin +90 -type f /var/www/grey.ca/cameras}
#do 
#cp /var/www/grey.ca/cameras/assets/unavailable.png i$
#done
###################################
###################################
############SecondScript###########
###################################
#!/bin/bash

#rm /usr/local/sbin/list.txt
cd /usr/local/sbin

ftp -nv <<EOF
open #Ftp ip
user #Username #Password

nlist *.jpg /usr/local/sbin/list.txt
bye
EOF

latest=$(tail -1 /usr/local/sbin/list.txt)

ftp -nv <<EOF
open #Ftp ip
user #Username #Password
binary
get $latest
prompt
mdelete *.jpg
bye
EOF

#rm -f /var/www/www.grey.ca/cameras/124-4.jpg
cp /usr/local/sbin/$latest /var/www/grey.ca/cameras/124-4.jpg