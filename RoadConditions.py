# Created by : Joel Meier
# Date: 4/3/2019
# Purpose: To update Road conditions on AGOL and 511 from infobite
# Version: 2.0
# Updated: updated to pull data directly from AGOL and to check 511. no longer storeing data locally except to process.
import geopandas as gpd
import pandas as pd
import requests
import logging
import logging.handlers as handlers
import traceback
import polyline
import xml.etree.ElementTree as et
import datetime
import pytz
from pandas import DataFrame
from shapely.geometry import Point, Polygon, LineString
from arcgis.gis import GIS
from copy import deepcopy

logPath = "C:\\Logs\\"
fileName = "RoadConditions.log"
logging.basicConfig(
    level=logging.INFO,  # options are: INFO, DEBUG, WARNING, ERROR, CRITICAL
    datefmt='%m/%d/%Y %I:%M:%S %p',
    format="%(asctime)s %(levelname)s: %(message)s",
    handlers=[
        logging.StreamHandler(),  # uncomment for logs to show in console.
        handlers.TimedRotatingFileHandler(
            logPath+fileName, when="d", interval=1,  backupCount=7)
    ])
logging.info("========================Starting==============================")
start_time = datetime.datetime.now()
ConditionDictionary = {0: "Not Reported", 1: "Bare and Dry", 2: "Bare and Wet", 3: "Partly Snow Covered", 4: "Snow Covered",
                       5: "Partly Snow Packed", 6: "Snow Packed", 7: "Partly Ice Covered", 8: "Ice Covered"}
VisibilityDictionary = {0: "Not Reported", 1: "Good", 2: "Fair", 3: "Poor"}
TimeZoneOffsetHours = datetime.datetime.now(
    pytz.timezone("Canada/Eastern")).strftime('%z')[2]
join = gpd.GeoDataFrame(columns=['Engineerin', 'geometry', 'Patrol', 'objectid',
                                 'PrimaryRoadCondition', 'SecondaryRoadCondition', 'Visibility', 'Drifting', 'DateTime'])
# In order to authenticate the requests to get the data first a token needs to be recevied.
s = requests.Session()
r = s.post(
    'URL1')

print(r)
token = r.text.split("\"")
# Token received from data feed and placed into parametes to sign in.
payload = {
    'auditId': token[3],
    'username': '',
    'password': ''}
s2 = s.post(
    'URL2',
    params=payload)
print(s2.text)  # should read: u'{"Success":true,"Message":"OK"}'
r2 = s.post(
    'URL3?div=8030',
    params=payload)
root = et.fromstring(r2.content)


df_cols = ['LocationType_511',
           'IssueType_511',
           'Description_511',
           'DateTime',
           'Location_511',
           'PrimaryRoadCondition',
           'SecondaryRoadCondition',
           'Visibility',
           'Geometry']
df_AGOL = pd.DataFrame(columns=['objectid', 'globalid', 'LocationSelect', 'Camera',
                                'PrimaryRoadCondition', 'SecondaryRoadCondition', 'Visibility',
                                'Drifting', 'DateTime', 'CreationDate', 'Creator', 'EditDate', 'Editor',
                                'SHAPE'])
df = pd.DataFrame(columns=df_cols)
try:  # this section goes through the xml from Mun511 and extracts the "issues"
    if root.findall('.//Issue') is not None:
        for x in root.findall('.//Issue'):
            IssueType = x.find('IssueType').text if x.find(
                'IssueType') is not None else ''
            PlanEndLocal = x.find('PlanEndLocal').text if x.find(
                'PlanEndLocal') is not None else ''
            Description = x.find('Description').text if x.find(
                'Description') is not None else ''
            Condition1 =  x.find('CustomFields')[0][1].text if x.find('CustomFields')[0][1] is not None else ''
            Condition2 = x.find('CustomFields')[1][1].text if x.find('CustomFields')[1][1] is not None else ''
            Visability = x.find('CustomFields')[2][1].textif x.find('CustomFields')[2][1] is not None else ''
            
            try:  # going through several of the date fields to try and get a Start time
                PlanStartLocal = x.find('PlanStartLocal').text
            except BaseException:
                try:
                    PlanStartLocal = x.find('DataLocalTime').text
                except BaseException:
                    PlanStartLocal = None
            if x.find('Polygon') == None:
                for y in x.iter('Location'):  # Issues may have multiple locations
                    Location = y.find('MainRoad').text if y.find(
                        'MainRoad') is not None else ''
                    LocationType = y.find('LocationType').text if y.find(
                        'LocationType') is not None else ''
                    GoogleEncodedPolyline = y.findtext(
                        './/GoogleEncodedPolyline')
                    array = []
                    if GoogleEncodedPolyline is not None and Description is not None and len(polyline.decode(GoogleEncodedPolyline)) > 1:
                        data = [LocationType,
                                IssueType,
                                Description,
                                PlanStartLocal,
                                Location,
                                Condition1,
                                Condition2,
                                Visability,
                                LineString(polyline.decode(GoogleEncodedPolyline, geojson=True))]
                        df = df.append(pd.Series(data, index=df_cols),
                                       ignore_index=True)

                    elif GoogleEncodedPolyline is not None and Description is not None and len(polyline.decode(GoogleEncodedPolyline, geojson=True)) == 1:
                        data = [LocationType,
                                IssueType,
                                Description,
                                PlanStartLocal,
                                Location,
                                Condition1,
                                Condition2,
                                Visability,
                                Point(polyline.decode(GoogleEncodedPolyline, geojson=True))]
                        df = df.append(pd.Series(data, index=df_cols),
                                       ignore_index=True)

            else:
                for y in x.findall('Polygon'):
                    data = ['',
                            '',
                            Description,
                            PlanStartLocal,
                            '',
                            Condition1,
                            Condition2,
                            Visability,
                            Polygon(polyline.decode(y.text, geojson=True))]
                    df = df.append(pd.Series(data, index=df_cols),
                                   ignore_index=True)
    gdf_from511 = gpd.GeoDataFrame(df, geometry='Geometry')
    gdf_from511.crs = {'init': 'epsg:4326'}
    gdf_from511['DateTime'] = pd.to_datetime(gdf_from511['DateTime'])
except:
    logging.error("Unable to complete 511 download\n " +
                  traceback.format_exc())
# Trying to do a spatial join between the AGOL survey layer and our local road shapefile
# Buffers the point features then joins them to the line based on intersection
try:
    road = gpd.read_file("C:\\Scripts\\ProcessingData\\roadsWGS84.shp")
    road = road[['Engineerin', 'geometry', 'Patrol']]
    road.crs = {'init': 'epsg:4326'}
    # how many hours in the past the script searches for
    searchdate = datetime.datetime.now() - datetime.timedelta(hours=4)
    #Login stored in windows credential manager
    gis = GIS(profile="AGOL", verify_cert=False)
    # survey123 conditions
    # public view of road condition survey123
    fc = gis.content.get('a3262ac11b314b0fa32ddc79416e8ebb')
    flayer = fc.layers[0]
    fset = flayer.query(out_sr=4326)
    df_AGOL = fset.sdf
    # need to update because AGOL stores time in UNIX but with timezone offset
    df_AGOL["DateTime"] = df_AGOL["DateTime"]
    df_AGOL = df_AGOL.loc[df_AGOL["DateTime"] > searchdate +
                          pd.DateOffset(hours=int(TimeZoneOffsetHours))]
    df_AGOL["DateTime"] = df_AGOL["DateTime"]
    # reset index so that the concat joins lines back up
    df_AGOL = df_AGOL.reset_index()

except:
    logging.error("Unable to download data from AGOL\n " +
                  traceback.format_exc())
# Checks if data coming from AGOL and 511 is empty or not.
# If not empty then joins them all together and gets the most recent data.
# If they are both empty then posts a warning and continues on
try:
    if not df_AGOL.empty:
        # Convert ESRI shape to simple X,Y in order to buffer
        df_coords = pd.DataFrame(df_AGOL.SHAPE.values.tolist())
        # Concatenates the x,y, back to the shapefile
        df_concat = pd.concat([df_AGOL, df_coords], axis=1)
        geometry = [Point(xy) for xy in zip(df_concat.x, df_concat.y)]
        df = df_concat.drop(['x', 'y', 'SHAPE', 'spatialReference'], axis=1)
        crs = {'init': 'epsg:4326'}
        gdfFromAGOL = gpd.GeoDataFrame(df, crs=crs, geometry=geometry)
        gdfFromAGOL['geometry'] = gdfFromAGOL.geometry.buffer(0.125)  # ~9.9km
        join = gpd.sjoin(road, gdfFromAGOL)
        join = join.sort_values('DateTime').drop_duplicates(
            'Engineerin', keep='last')  # Keep last is what only takes the most recent if there is overlap
        join.drop('index_right', axis=1, inplace=True)

    # May need to change below to also only accept most recent
    if not gdf_from511.empty:
        join2 = gpd.sjoin(road, gdf_from511)

    if not gdf_from511.empty and not df_AGOL.empty:
        join = pd.merge(
            left=join2,
            right=join,
            how='outer',
            left_on='Engineerin',
            right_on='Engineerin')

    if not gdf_from511.empty and df_AGOL.empty:
        join = join2

    if df_AGOL.empty and gdf_from511.empty:
        logging.warn("No Data from either 511 or Survey123")

    gis = GIS(
        profile="AGOL", verify_cert=False)  # credentials stored in windows identity manager
    # Road condition Public Map
    fc2 = gis.content.get('59a6668cf4944fdf9a5e434566917be1') #road condition layer
    flayer2 = fc2.layers[0]
    fset2 = flayer2.query(out_sr=4326)
    fields = ('GPS_Time', 'Visibility', 'EngineeringSection', 'Condition')
    logging.info("New section defined")
    # join the new table to the feature class stored in AGOL
    overlap_rows = pd.merge(
        left=fset2.sdf,
        right=join,
        how='inner',
        left_on='EngineeringSection',
        right_on='Engineerin')
    features_for_update = []
    all_features = fset2.features
    logging.info("Layers joined")
    # reset all features to blank
    for f in fset2.features:
        if f.attributes['GPS_Time'] != None:
            f.attributes['GPS_Time'] = None
            f.attributes['Visibility'] = None
            f.attributes['Condition'] = None
            f.attributes['SecondaryRoadConditions'] = None
            flayer2.edit_features(updates=[f])
    # Update rows of needed features
    # If join is empty it just skips this part
    for EngSec in join['Engineerin']:
        original_feature = [
            f for f in all_features if f.attributes['EngineeringSection'] == EngSec]
        feature_to_be_updated = deepcopy(original_feature[0])
        matching_row = join.loc[join['Engineerin'] == EngSec]
        feature_to_be_updated.attributes['GPS_Time'] = matching_row.iloc[0]['DateTime']
        feature_to_be_updated.attributes['Visibility'] = matching_row.iloc[0]['Visibility']
        feature_to_be_updated.attributes['Condition'] = str(
            matching_row.iloc[0]['PrimaryRoadCondition'])
        feature_to_be_updated.attributes['SecondaryRoadCondition'] = str(
            matching_row.iloc[0]['SecondaryRoadCondition'])
        update_result = flayer2.edit_features(updates=[feature_to_be_updated])
        update_result
    logging.info("AGOL road layer updated")
    # Push the request out to Municipal511
    s = requests.Session()
    header = {
        'Auth-Username': '',  
        'Auth-Password': '',
        'Content-Type': 'application/json'}
    data = {"UploadRequest": {"Projection": 1,
                              "DataLayer": 8030,
                              "Issues": []}}
    url = "UploadURL"
    # The following for loop oganizes the data so that it is in the format 511 will accept
    for f in fset2.features:
        if f.attributes['GPS_Time'] is not None:
            geom = f.geometry['paths'][0]
            geomarray = []
            for coord in geom:
                geomarray.append({'X': coord[0], 'Y': coord[1]})
            Issues = {"Locations": [{"Geometry": geomarray,
                                     "Description": f.attributes["Condition"]}],
                      "Type": 1,
                      "Priority": 3,
                      "SourceId": f.attributes['OBJECTID'],
                      "StartTime": f.attributes['GPS_Time'],
                      "Url": "https://www.grey.ca",
                      "Description": f.attributes['Location']}
            data['UploadRequest']['Issues'].append(Issues)
    s2 = s.post(url, headers=header, json=data)  # pushing the Data to 511
    logging.info("511 road layer updated"+str(s2))

    # updating the Patrol weather conditions
    # Patrol Area weather conditions (used for reporting text based conditions)
    PatrolFC = gis.content.get('947bfddc97cb4153a0385040c2233a4a')
    PatrolFClayer = PatrolFC.layers[0]
    PatrolFCset = PatrolFClayer.query(out_sr=4326)
    PatrolDf = PatrolFCset.sdf
    PatrolDF = DataFrame(join.dissolve(
        by=['Patrol', 'PrimaryRoadCondition'], aggfunc='sum').length)
    PatrolDF = PatrolDF.reset_index()
    PatrolDF = PatrolDF.rename(columns={
        'Patrol': 'Patrol', 'PrimaryRoadCondition': 'PrimaryRoadCondition', 0: 'Length'})
    Patrols = ['A', 'B', 'C', 'D']

    for f in PatrolFCset.features:
        f.attributes['SecCon'] = None
        tm = datetime.datetime.now()+pd.DateOffset(hours=int(TimeZoneOffsetHours)
                                                   )  # current time adjusted for timezone
        tz = datetime.timedelta(minutes=tm.minute % 10,
                                seconds=tm.second, microseconds=tm.microsecond)  # difference between current time and rounding to 10minutes
        # adding timezone offset to fix same UNIX issue as before
        # when not reported the updated time will be rounded to make it look better
        f.attributes['LastUpdated'] = str(tm-tz)
        f.attributes['Visibility'] = "Not Reported"
        f.attributes['Condition'] = "Not Reported"
        PatrolFClayer.edit_features(updates=[f])
    # When there is data to be updated it does a groupby for each patrol
    if not join.empty:
        for f in PatrolFCset.features:
            for Patrol in Patrols:
                if f.attributes['Patrol'] == Patrol:
                    matching_row = PatrolDF.loc[PatrolDF['Patrol'] == Patrol]
                    main_matching_row = join.loc[join['Patrol'] == Patrol]
                    # need to use a sperate table to get the most prevelant as opposed to most severe
                    if matching_row.empty == False and main_matching_row.empty == False:
                        # Gets the condition that occures on the most roads by length as the primary
                        f.attributes['Condition'] = ConditionDictionary[int(matching_row.loc[matching_row['Length'].idxmax(
                        )]['PrimaryRoadCondition'])]
                        # Gets the worst visibility
                        f.attributes['Visibility'] = int(
                            main_matching_row['Visibility'].max())
                        # Gets the most recent updated time
                        f.attributes['LastUpdated'] = main_matching_row['DateTime'].max(
                        )#-pd.DateOffset(hours=int(TimeZoneOffsetHours))
                        if len(main_matching_row['SecondaryRoadCondition'].value_counts()) > 0:
                            try:
                                if int(main_matching_row['SecondaryRoadCondition'].fillna('0').apply(max).max()) != int(matching_row.loc[matching_row['Length'].idxmax()]['PrimaryRoadCondition']):
                                    f.attributes['SecCon'] = ConditionDictionary[int(main_matching_row['SecondaryRoadCondition'].apply(
                                        max).max())]  # only apply secondary condition if it's less severe than primary
                            except:
                                f.attributes['SecCon'] = None
                        else:
                            f.attributes['SecCon'] = None
                    else:
                        f.attributes['SecCon'] = None
                    PatrolFClayer.edit_features(updates=[f])
except:
    logging.error(traceback.format_exc())
finally:
    totaltime = datetime.datetime.now() - start_time
    # processing time is an issue with this script, expect 40 seconds
    logging.info("Total processing time: " + str(totaltime) + "\n")
    sys.exit(0)