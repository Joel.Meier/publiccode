import requests
import json
import time
import arcpy
import polyline
s = requests.Session()
r = s.get('https://511on.ca/api/v2/get/roadconditions?format=json')
print r
root = json.loads(r.text)
Locations={'From Flesherton to Shelburne','From Chatsworth to Flesherton','From Collingwood to Owen Sound','From Mount Forest to Owen Sound','From Underwood to Springmount','From Rosemont to Harriston','From Shelburne to Orangeville','From Owen Sound to Tobermory'}
ColourCode={"Bare and dry road" : 1, "Bare and wet road": 1,  "Partly snow covered":2,"Partly snow covered":2,"Partly ice covered":2,"Partly snow packed":2 ,"Snow covered":3,"Snow packed":3 ,"Ice covered":3}   
fcl = "C:/Users/..../AppData/Roaming/Esri/Desktop10.6/ArcCatalog/GISWeb.sde/MTORoadConditions"
arcpy.DeleteRows_management(fcl)
for loc in Locations:
    LocationDescription = next(item for item in root if item["LocationDescription"].split('|')[0] == loc)["LocationDescription"].split('|')[0]
    Condition = next(item for item in root if item["LocationDescription"].split('|')[0] == loc)["Condition"][:254]
    Visibility = next(item for item in root if item["LocationDescription"].split('|')[0] == loc)["Visibility"]
    RoadwayName = next(item for item in root if item["LocationDescription"].split('|')[0] == loc)["RoadwayName"]
    Drifting = next(item for item in root if item["LocationDescription"].split('|')[0] == loc)["Drifting"]
    EncodedPolyline = next(item for item in root if item["LocationDescription"].split('|')[0] == loc)["EncodedPolyline"]
    Colourcode = ColourCode[Condition[0]]
    print LocationDescription,Condition,Visibility,RoadwayName,Drifting,ColourCode
    cur = arcpy.da.InsertCursor(fcl, ["LocationDescription","Condition","Visibility","RoadwayName","Drifting","ColourCode","SHAPE@"])
    array = arcpy.Array()
    for coords in polyline.decode(EncodedPolyline):
        array.add(arcpy.Point(coords[1], coords[0]))
    cur.insertRow([LocationDescription,str(Condition).replace("u'","").replace("'","").replace("[","").replace("]",""),Visibility,RoadwayName,Drifting,int(Colourcode),arcpy.Polyline(array)])

    
s2 = requests.Session()
r2 = s2.get('https://511on.ca/api/v2/get/constructionprojects?format=json')
fcp = "C:/Users/..../AppData/Roaming/Esri/Desktop10.6/ArcCatalog/GISWeb.sde/MTOConstruction"
arcpy.DeleteRows_management(fcp)
print r2
root2 = json.loads(r2.text)
for item in root2:
    if float(item["Latitude"]) > 44.047286 and float(item["Latitude"]) < 44.860453 and float(item["Latitude"]) < -80.169539 and float(item["Latitude"]) > -81.209162:
        RoadwayName = item['RoadwayName']
        StartDate = item['StartDate']
        Description = item['Description']
        PlannedEndDate = item['PlannedEndDate']
        EventType = item['EventType']
        LanesAffected = item['LanesAffected']
        print (RoadwayName)
        cur = arcpy.da.InsertCursor(fcp, ["RoadwayName","Description","StartDate","PlannedEndDate","EventType","LanesAffected","SHAPE@"])
        cur.insertRow([RoadwayName,Description,StartDate,PlannedEndDate,EventType,LanesAffected,arcpy.Point(Longitude.text,Latitude.text)])
