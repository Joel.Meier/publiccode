import requests
from lxml import etree
import lxml
import pandas as pd
import polyline
import geopandas as gpd
from shapely.geometry import Point, LineString, Polygon

s = requests.Session()
r = s.post(
    'URL1')
print(r)
token = r.text.split("\"")
payload = {
    'auditId': token[3],
    'username': '',
    'password': ''}
s2 = s.post(
    'URL2',
    params=payload)
print(s2.text)  # should read: u'{"Success":true,"Message":"OK"}'
###Layer list seperated by bar###
#5000		Road Issues (Public)
#5001		Road Issues (Not public)
#5002		Training/Demonstration
#8008		Emergency Access Points
#8010		Road Restrictions/Cautions (Public)
#8013		Permitted Burning
#8015		Incidents/Events
#8016		Persistent Hazards
#8017		Key Infrastructure
#8019		Exercise
#8021		Response Resources
#8023		Fire Services
#8030		Winter Road Conditions (Public)
#8048		Staging
#8061		Road Permits/Volunteers
#8063		Emergency (Public)

r = s.post(
    'URL3?div=5000|8010',#These can be changed for whatever layers you'd like, list of layers is above
    params=payload)
print(r)
root = etree.fromstring(r.content)
cols = ['Description', 'LocationType', 'IssueType',
        'Location', 'MainRoad', 'PlanStartLocal', 'PlanEndLocal']
tags = {"tags": []}
#testing: ((Hour([dbo_ACRTime].[TimeMaxDateTime])*60)+Minute([dbo_ACRTime].[TimeMaxDateTime]))

def getattibs(x):
    tag = {}
    try:
        tag["LocationType"] = x.find('LocationType').text
    except:
        tag["LocationType"] = None
    try:
        tag["Description"] = x.getparent().find('Description').text
    except:
        tag["Description"] = None
    try:
        tag["IssueType"] = x.getparent().find('IssueType').text
    except:
        tag["IssueType"] = None
    try:
        tag["Location"] = x.find('GoogleEncodedPolyline').text
        tag["Shape"] = 'Line'
    except:
        try:
            tag["Location"] = x.find('Polygon').text
            tag["Shape"] = 'Polygon'
        except:
            tag["Location"] = None
    try:
        tag["MainRoad"] = x.find('MainRoad').text
    except:
        tag["MainRoad"] = None
    try:
        tag["PlanStartLocal"] = x.getparent().find('PlanStartLocal').text
    except:
        try:
            tag["PlanStartLocal"] = x.getparent().find('DataLocalTime').text
        except:
            tag["PlanStartLocal"] = None
    try:
        tag["PlanEndLocal"] = x.getparent().find('PlanEndLocal').text
    except:
        tag["PlanEndLocal"] = None

    tags["tags"]. append(tag)


for x in root.findall('.//Location'):
    getattibs(x)


for x in root.findall('.//Polygon'):
    getattibs(x.getparent())


df = pd.DataFrame(tags["tags"])
df["Geometry"] = df['Location'].apply(
    lambda x: (polyline.decode(x, geojson=True)))
df.loc[df["Geometry"].str.len() == 1, 'Shape'] = 'Point'


dfline = df.loc[df["Shape"] == "Line"]
Linegeom = dfline["Geometry"].apply(lambda x: LineString(x))
Linegdf = gpd.GeoDataFrame(dfline, geometry=Linegeom)
del Linegdf['Geometry']

dfPoint = df.loc[df["Shape"] == "Point"]
Pointgeom = dfPoint["Geometry"].apply(lambda x: Point(x))
Pointgdf = gpd.GeoDataFrame(dfPoint, geometry=Pointgeom)
del Pointgdf['Geometry']

dfPoly = df.loc[df["Shape"] == "Polygon"]
Polygeom = dfPoly["Geometry"].apply(lambda x: Polygon(x))
Polygdf = gpd.GeoDataFrame(dfPoly, geometry=Polygeom)
del Polygdf['Geometry']

Polygdf.to_file('C:\\Users\\User\\Desktop\\Mun511Poly.shp', driver='ESRI Shapefile')
Pointgdf.to_file('C:\\Users\\User\\Desktop\\Mun511Point.shp', driver='ESRI Shapefile')
Linegdf.to_file('C:\\Users\\User\\Desktop\\Mun511Line.shp', driver='ESRI Shapefile')
